################################################################################
# Optional Variables
################################################################################

variable "host" {
  type        = string
  default     = "truststore.pki.rds.amazonaws.com"
  description = "The host that is holding AWS RDS CA information."
}

variable "region" {
  type        = string
  default     = null
  description = "Which region should we get the AWS RDS CA information for. By default the global bundle retrieved."
}

variable "protocol" {
  type        = string
  default     = "https"
  description = "The protocol used by the host that is holding AWS RDS CA information."

  validation {
    condition     = contains(["http", "https"], var.protocol)
    error_message = "Certificate host protocol must be either 'http' or 'https'."
  }
}

################################################################################

variable "path_global" {
  type        = string
  default     = "global/global-bundle.pem"
  description = "Path to the global certificate bundle."
}

################################################################################

variable "retry_count" {
  type        = number
  default     = 3
  description = "Number of times to retry downloading the CA bundle(s)."
}

variable "retry_delay_max" {
  type        = number
  default     = 10000
  description = "Maximum delay between retries in milliseconds."
}

variable "retry_delay_min" {
  type        = number
  default     = 1000
  description = "Minimum delay between retries in milliseconds."
}

################################################################################
# Locals
################################################################################

locals {
  url    = format("%s://%s/%s", var.protocol, var.host, local.path)
  path   = local.global ? var.path_global : format("%s/%s-bundle.pem", var.region, var.region)
  global = var.region == null
}

################################################################################
# Data Sources
################################################################################

data "http" "scope" {
  url = local.url

  retry {
    attempts     = var.retry_count
    max_delay_ms = var.retry_delay_max
    min_delay_ms = var.retry_delay_min
  }

  lifecycle {
    postcondition {
      condition     = self.status_code == 200
      error_message = "Invalid HTTP status getting AWS CA bundle!"
    }
  }
}

################################################################################
# Outputs
################################################################################

output "host" {
  value = var.host
}

output "region" {
  value = var.region
}

output "protocol" {
  value = var.protocol
}

################################################################################

output "path_global" {
  value = var.path_global
}

################################################################################

output "retry_count" {
  value = var.retry_count
}

output "retry_delay_max" {
  value = var.retry_delay_max
}

output "retry_delay_min" {
  value = var.retry_delay_min
}

################################################################################

output "url" {
  value = local.url
}

output "path" {
  value = local.path
}

output "global" {
  value = local.global
}

################################################################################

output "bundle" {
  value = data.http.scope.response_body
}

output "status" {
  value = data.http.scope.status_code
}

output "headers" {
  value = data.http.scope.response_headers
}

################################################################################
