<!----------------------------------------------------------------------------->

# rds/certificate

#### Gathers certificate authority information for [RDS]

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/database/aws//rds/certificate`**

--------------------------------------------------------------------------------

### Example Usage

```
module "my_certificate" {
  source = "gitlab.com/bitservices/database/aws//rds/certificate"
}

output "my_certificate_bundle" {
  value = module.my_certificate.bundle
}
```

<!----------------------------------------------------------------------------->

[RDS]: https://aws.amazon.com/rds

<!----------------------------------------------------------------------------->
