################################################################################
# Optional Variables
################################################################################

variable "snapshot_most_recent" {
  type        = bool
  default     = true
  description = "When 'restore' is 'true' but 'restore_id' is not set, search for the latest snapshot that matches this RDS instances identifier. Should always be 'true'."
}

################################################################################
# Locals
################################################################################

locals {
  snapshot_do_lookup = var.restore ? var.restore_id == "" : false
}

################################################################################
# Data Sources
################################################################################

data "aws_db_snapshot" "scope" {
  count                  = local.snapshot_do_lookup ? 1 : 0
  most_recent            = var.snapshot_most_recent
  db_instance_identifier = var.name
}

################################################################################
# Outputs
################################################################################

output "snapshot_id" {
  value = length(data.aws_db_snapshot.scope) == 1 ? data.aws_db_snapshot.scope[0].db_snapshot_identifier : null
}

output "snapshot_arn" {
  value = length(data.aws_db_snapshot.scope) == 1 ? data.aws_db_snapshot.scope[0].db_snapshot_arn : null
}

output "snapshot_engine" {
  value = length(data.aws_db_snapshot.scope) == 1 ? data.aws_db_snapshot.scope[0].engine : null
}

output "snapshot_status" {
  value = length(data.aws_db_snapshot.scope) == 1 ? data.aws_db_snapshot.scope[0].status : null
}

output "snapshot_create_time" {
  value = length(data.aws_db_snapshot.scope) == 1 ? data.aws_db_snapshot.scope[0].snapshot_create_time : null
}

output "snapshot_database_id" {
  value = length(data.aws_db_snapshot.scope) == 1 ? data.aws_db_snapshot.scope[0].db_instance_identifier : null
}

output "snapshot_storage_iops" {
  value = length(data.aws_db_snapshot.scope) == 1 ? data.aws_db_snapshot.scope[0].iops : null
}

output "snapshot_storage_size" {
  value = length(data.aws_db_snapshot.scope) == 1 ? data.aws_db_snapshot.scope[0].allocated_storage : null
}

output "snapshot_engine_version" {
  value = length(data.aws_db_snapshot.scope) == 1 ? data.aws_db_snapshot.scope[0].engine_version : null
}

output "snapshot_storage_encrypted" {
  value = length(data.aws_db_snapshot.scope) == 1 ? data.aws_db_snapshot.scope[0].encrypted : null
}

################################################################################
