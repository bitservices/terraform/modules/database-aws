<!----------------------------------------------------------------------------->

# rds/instance

#### Manages [RDS] database instances

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/database/aws//rds/instance`**

--------------------------------------------------------------------------------

### Example Usage

```
variable "vpc"     { default = "sandpit01"                }
variable "owner"   { default = "terraform@bitservices.io" }
variable "company" { default = "BITServices Ltd"          }

resource "random_password" "my_password" {
  length  = 32
  special = false
}

module "my_subnet_group" {
  source  = "gitlab.com/bitservices/database/aws//rds/subnet-group"
  vpc     = var.vpc
  owner   = var.owner
  company = var.company
}

module "my_mysql80_parameters" {
  source  = "gitlab.com/bitservices/database/aws//rds/parameter-group"
  owner   = var.owner
  family  = "mysql8.0"
  company = var.company
}

module "my_instance" {
  source            = "gitlab.com/bitservices/database/aws//rds/instance"
  vpc               = var.vpc
  name              = "foo-bar"
  owner             = var.owner
  company           = var.company
  username          = "username"
  password          = random_password.my_password.result
  parameters        = module.my_mysql80_parameters.id
  subnet_group_name = module.my_subnet_group.name
}
```

<!----------------------------------------------------------------------------->

[RDS]: https://aws.amazon.com/rds

<!----------------------------------------------------------------------------->
