################################################################################
# Required Variables
################################################################################

variable "name" {
  type        = string
  description = "The full name of this RDS instance."
}

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

################################################################################

variable "username" {
  type        = string
  description = "Username for the primary database user."
}

variable "password" {
  type        = string
  sensitive   = true
  description = "Password for the primary database user."
}

variable "parameters" {
  type        = string
  description = "Name of the database parameter group to associate."
}

################################################################################
# Optional Variables
################################################################################

variable "ca" {
  type        = string
  default     = "rds-ca-ecc384-g1"
  description = "The identifier of the CA certificate for the DB instance."
}

variable "type" {
  type        = string
  default     = "db.t4g.micro"
  description = "The instance type of the RDS instance."
}

variable "zone" {
  type        = string
  default     = "*"
  description = "The availability zone of this RDS instance. '*' enables any/all within the subnet group and enables multi availability zone."
}

variable "public" {
  type        = bool
  default     = false
  description = "Control if instance is publicly accessible."
}

variable "database" {
  type        = string
  default     = null
  description = "The name of the database to create when the RDS instance is created. If not specified, no database is created."
}

variable "auto_upgrade" {
  type        = bool
  default     = false
  description = "Indicates that minor engine upgrades will be applied automatically to the RDS instance during the maintenance window."
}

variable "license_model" {
  type        = string
  default     = null
  description = "License model information for this RDS instance. Defaults to 'postgresql-license' if 'engine' is 'postgres', otherwise the default is 'general-public-license'."
}

variable "apply_immediately" {
  type        = bool
  default     = true
  description = "Specifies whether any database modifications are applied immediately, or during the next maintenance window."
}

variable "maintenance_window" {
  type        = string
  default     = "mon:04:00-mon:05:00"
  description = "The window to perform maintenance in. Syntax: 'ddd:hh24:mi-ddd:hh24:mi'."
}

variable "final_snapshot_name" {
  type        = string
  default     = null
  description = "The name of your final RDS snapshot when this RDS instance is deleted."
}

variable "skip_final_snapshot" {
  type        = bool
  default     = false
  description = "Determines whether a final RDS snapshot is created before the RDS instance is deleted."
}

variable "copy_tags_to_snapshot" {
  type        = bool
  default     = true
  description = "On delete, copy all instance tags to the final snapshot (if 'final_snapshot_name' is specified)."
}

################################################################################

variable "backup_window" {
  type        = string
  default     = "02:00-03:00"
  description = "The daily time range (in UTC) during which automated backups are created if they are enabled. Must not overlap with 'maintenance_window'."
}

variable "backup_retention_days" {
  type        = number
  default     = 1
  description = "How long in days to retain backups for."
}

################################################################################

variable "restore" {
  type        = bool
  default     = false
  description = "If 'true', will create this RDS instance from a previously created snapshot."
}

variable "restore_id" {
  type        = string
  default     = null
  description = "The snapshot identifier to restore from if 'restore' is 'true'. If blank, a snapshot from a RDS instance matching the identifier of this RDS instance is used."
}

################################################################################

variable "storage_size" {
  type        = number
  default     = 20
  description = "The amount of allocated storage in GB."
}

variable "storage_type" {
  type        = string
  default     = "standard"
  description = "The type of storage for this RDS instance. Can be 'standard', 'gp3', 'gp2', 'io2' or 'io1'."

  validation {
    condition     = contains(["standard", "gp3", "gp2", "io2", "io1"], var.storage_type)
    error_message = "The storage for the RDS instance must be of type: 'standard', 'gp3', 'gp2', 'io2' or 'io1'."
  }
}

################################################################################

variable "subnet_set" {
  type        = string
  default     = "default"
  description = "Which set of subnets the subnet group that will be attached to this RDS instance belong too. Ignored if 'subnet_group_name' is set."
}

variable "subnet_tier" {
  type        = string
  default     = "backend"
  description = "Which tier of subnets the subnet group that will be attached to this RDS instance belong too. Ignored if 'subnet_group_name' is set."
}

variable "subnet_group_name" {
  type        = string
  default     = null
  description = "Full name of the subnet group to attach this RDS instance. Must be specified if subnet group is created in the same Terraform run."
}

variable "subnet_group_class" {
  type        = string
  default     = "default"
  description = "Identifier of the subnet group to attach this RDS instance to within 'subnet_set' and 'subnet_tier'. Ignored if 'subnet_group_name' is set."
}

################################################################################
# Locals
################################################################################

locals {
  multi_az            = var.zone == "*" ? true : false
  restore_id          = var.restore ? coalesce(var.restore_id, join("", data.aws_db_snapshot.scope.*.db_snapshot_identifier)) : null
  license_model       = coalesce(var.license_model, var.engine == "postgres" ? "postgresql-license" : "general-public-license")
  subnet_group_name   = coalesce(var.subnet_group_name, format("%s-%s-%s-%s", var.vpc, var.subnet_tier, var.subnet_set, var.subnet_group_class))
  final_snapshot_name = coalesce(var.final_snapshot_name, format("%s-final-%s", var.name, replace(timestamp(), ":", "-")))
}

################################################################################
# Resources
################################################################################

resource "aws_db_instance" "scope" {
  engine                     = data.aws_rds_engine_version.scope.engine
  db_name                    = var.database
  multi_az                   = local.multi_az
  password                   = var.password
  username                   = var.username
  identifier                 = var.name
  kms_key_id                 = local.storage_encryption_key
  storage_type               = var.storage_type
  backup_window              = var.backup_window
  license_model              = local.license_model
  engine_version             = data.aws_rds_engine_version.scope.version
  instance_class             = var.type
  allocated_storage          = var.storage_size
  apply_immediately          = var.apply_immediately
  storage_encrypted          = var.storage_encryption_enabled
  availability_zone          = var.zone != "*" ? format("%s%s", data.aws_region.scope.name, var.zone) : null
  ca_cert_identifier         = var.ca
  maintenance_window         = var.maintenance_window
  publicly_accessible        = var.public
  skip_final_snapshot        = var.skip_final_snapshot
  snapshot_identifier        = local.restore_id
  parameter_group_name       = var.parameters
  db_subnet_group_name       = local.subnet_group_name
  copy_tags_to_snapshot      = var.copy_tags_to_snapshot
  vpc_security_group_ids     = concat(var.security_group_ids, data.aws_security_group.scope.*.id)
  backup_retention_period    = var.backup_retention_days
  final_snapshot_identifier  = local.final_snapshot_name
  auto_minor_version_upgrade = var.auto_upgrade

  tags = {
    "CA"         = var.ca
    "VPC"        = var.vpc
    "Name"       = var.name
    "Zone"       = var.zone == "*" ? "multi" : var.zone
    "Owner"      = var.owner
    "Engine"     = data.aws_rds_engine_version.scope.engine
    "Public"     = var.public ? "Yes" : "No"
    "Region"     = data.aws_region.scope.name
    "Company"    = var.company
    "Storage"    = format("%v GB", var.storage_size)
    "Version"    = data.aws_rds_engine_version.scope.version
    "Database"   = var.database
    "Username"   = var.username
    "Encryption" = var.storage_encryption_enabled ? "Enabled" : "Disabled"
  }

  lifecycle {
    ignore_changes = [
      snapshot_identifier,
      final_snapshot_identifier
    ]
  }
}

################################################################################
# Outputs
################################################################################

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

################################################################################

output "parameters" {
  value = var.parameters
}

################################################################################

output "auto_upgrade" {
  value = var.auto_upgrade
}

output "apply_immediately" {
  value = var.apply_immediately
}

output "final_snapshot_name" {
  value = local.final_snapshot_name
}

output "skip_final_snapshot" {
  value = var.skip_final_snapshot
}

output "copy_tags_to_snapshot" {
  value = var.copy_tags_to_snapshot
}

################################################################################

output "restore" {
  value = var.restore
}

output "restore_id" {
  value = local.restore_id
}

################################################################################

output "storage_type" {
  value = var.storage_type
}

################################################################################

output "subnet_set" {
  value = var.subnet_set
}

output "subnet_tier" {
  value = var.subnet_tier
}

output "subnet_group_class" {
  value = var.subnet_group_class
}

################################################################################

output "ca" {
  value = aws_db_instance.scope.ca_cert_identifier
}

output "id" {
  value = aws_db_instance.scope.id
}

output "arn" {
  value = aws_db_instance.scope.arn
}

output "name" {
  value = aws_db_instance.scope.identifier
}

output "port" {
  value = aws_db_instance.scope.port
}

output "type" {
  value = aws_db_instance.scope.instance_class
}

output "zone" {
  value = aws_db_instance.scope.availability_zone
}

output "public" {
  value = aws_db_instance.scope.publicly_accessible
}

output "engine" {
  value = aws_db_instance.scope.engine
}

output "status" {
  value = aws_db_instance.scope.status
}

output "address" {
  value = aws_db_instance.scope.address
}

output "zone_id" {
  value = aws_db_instance.scope.hosted_zone_id
}

output "database" {
  value = aws_db_instance.scope.db_name
}

output "endpoint" {
  value = aws_db_instance.scope.endpoint
}

output "multi_az" {
  value = aws_db_instance.scope.multi_az
}

output "username" {
  value = aws_db_instance.scope.username
}

output "password" {
  sensitive = true
  value     = aws_db_instance.scope.password
}

output "storage_size" {
  value = aws_db_instance.scope.allocated_storage
}

output "license_model" {
  value = aws_db_instance.scope.license_model
}

output "backup_window" {
  value = aws_db_instance.scope.backup_window
}

output "engine_version" {
  value = aws_db_instance.scope.engine_version
}

output "subnet_group_name" {
  value = aws_db_instance.scope.db_subnet_group_name
}

output "maintenance_window" {
  value = aws_db_instance.scope.maintenance_window
}

output "backup_retention_days" {
  value = aws_db_instance.scope.backup_retention_period
}

output "storage_encryption_key" {
  value = aws_db_instance.scope.kms_key_id
}

output "storage_encryption_enabled" {
  value = aws_db_instance.scope.storage_encrypted
}

################################################################################
