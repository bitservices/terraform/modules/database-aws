################################################################################
# Optional Variables
################################################################################

variable "engine" {
  type        = string
  default     = "mysql"
  description = "The database engine."
}

variable "engine_version" {
  type        = string
  default     = "8.0"
  description = "The database engine version."
}

################################################################################
# Data Sources
################################################################################

data "aws_rds_engine_version" "scope" {
  engine       = var.engine
  version      = var.engine_version
  default_only = true
}

################################################################################
# Outputs
################################################################################

output "engine_log" {
  value = data.aws_rds_engine_version.scope.supports_log_exports_to_cloudwatch
}

output "engine_modes" {
  value = data.aws_rds_engine_version.scope.supported_modes
}

output "engine_global" {
  value = data.aws_rds_engine_version.scope.supports_global_databases
}

output "engine_status" {
  value = data.aws_rds_engine_version.scope.status
}

output "engine_updates" {
  value = data.aws_rds_engine_version.scope.valid_upgrade_targets
}

output "engine_features" {
  value = data.aws_rds_engine_version.scope.supported_feature_names
}

output "engine_parallel" {
  value = data.aws_rds_engine_version.scope.supports_parallel_query
}

output "engine_log_types" {
  value = data.aws_rds_engine_version.scope.exportable_log_types
}

output "engine_timezones" {
  value = data.aws_rds_engine_version.scope.supported_timezones
}

output "engine_description" {
  value = data.aws_rds_engine_version.scope.engine_description
}

output "engine_read_replicas" {
  value = data.aws_rds_engine_version.scope.supports_read_replica
}

output "engine_character_set_list" {
  value = data.aws_rds_engine_version.scope.supported_character_sets
}

output "engine_version_description" {
  value = data.aws_rds_engine_version.scope.version_description
}

output "engine_character_set_default" {
  value = data.aws_rds_engine_version.scope.default_character_set
}

################################################################################
