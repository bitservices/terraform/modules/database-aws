################################################################################
# Optional Variables
################################################################################

variable "storage_encryption_key" {
  type        = string
  default     = null
  description = "KMS key to use for server side encryption. Accepts key ARN, ID, alias or alias ARN as inputs."
}

variable "storage_encryption_enabled" {
  type        = bool
  default     = true
  description = "Specifies whether the RDS instance is encrypted."
}

################################################################################
# Locals
################################################################################

locals {
  storage_encryption_key    = local.storage_encryption_lookup ? data.aws_kms_key.storage_encryption_key[0].arn : null
  storage_encryption_lookup = var.storage_encryption_enabled && var.storage_encryption_key != null
}

################################################################################
# Data Sources
################################################################################

data "aws_kms_key" "storage_encryption_key" {
  count  = local.storage_encryption_lookup ? 1 : 0
  key_id = var.storage_encryption_key
}

################################################################################
# Outputs
################################################################################

output "storage_encryption_lookup" {
  value = local.storage_encryption_lookup
}

################################################################################

output "storage_encryption_key_id" {
  value = length(data.aws_kms_key.storage_encryption_key) == 1 ? data.aws_kms_key.storage_encryption_key[0].id : null
}

output "storage_encryption_key_xks" {
  value = length(data.aws_kms_key.storage_encryption_key) == 1 ? data.aws_kms_key.storage_encryption_key[0].xks_key_configuration : null
}

output "storage_encryption_key_spec" {
  value = length(data.aws_kms_key.storage_encryption_key) == 1 ? data.aws_kms_key.storage_encryption_key[0].key_spec : null
}

output "storage_encryption_key_state" {
  value = length(data.aws_kms_key.storage_encryption_key) == 1 ? data.aws_kms_key.storage_encryption_key[0].key_state : null
}

output "storage_encryption_key_store" {
  value = length(data.aws_kms_key.storage_encryption_key) == 1 ? data.aws_kms_key.storage_encryption_key[0].custom_key_store_id : null
}

output "storage_encryption_key_usage" {
  value = length(data.aws_kms_key.storage_encryption_key) == 1 ? data.aws_kms_key.storage_encryption_key[0].key_usage : null
}

output "storage_encryption_key_origin" {
  value = length(data.aws_kms_key.storage_encryption_key) == 1 ? data.aws_kms_key.storage_encryption_key[0].origin : null
}

output "storage_encryption_key_account" {
  value = length(data.aws_kms_key.storage_encryption_key) == 1 ? data.aws_kms_key.storage_encryption_key[0].aws_account_id : null
}

output "storage_encryption_key_enabled" {
  value = length(data.aws_kms_key.storage_encryption_key) == 1 ? data.aws_kms_key.storage_encryption_key[0].enabled : null
}

output "storage_encryption_key_manager" {
  value = length(data.aws_kms_key.storage_encryption_key) == 1 ? data.aws_kms_key.storage_encryption_key[0].key_manager : null
}

output "storage_encryption_key_creation" {
  value = length(data.aws_kms_key.storage_encryption_key) == 1 ? data.aws_kms_key.storage_encryption_key[0].creation_date : null
}

output "storage_encryption_key_deletion" {
  value = length(data.aws_kms_key.storage_encryption_key) == 1 ? data.aws_kms_key.storage_encryption_key[0].deletion_date : null
}

output "storage_encryption_key_description" {
  value = length(data.aws_kms_key.storage_encryption_key) == 1 ? data.aws_kms_key.storage_encryption_key[0].description : null
}

output "storage_encryption_key_hsm_cluster" {
  value = length(data.aws_kms_key.storage_encryption_key) == 1 ? data.aws_kms_key.storage_encryption_key[0].cloud_hsm_cluster_id : null
}

output "storage_encryption_key_master_spec" {
  value = length(data.aws_kms_key.storage_encryption_key) == 1 ? data.aws_kms_key.storage_encryption_key[0].customer_master_key_spec : null
}

output "storage_encryption_key_deletion_window" {
  value = length(data.aws_kms_key.storage_encryption_key) == 1 ? data.aws_kms_key.storage_encryption_key[0].pending_deletion_window_in_days : null
}

output "storage_encryption_key_expiration_date" {
  value = length(data.aws_kms_key.storage_encryption_key) == 1 ? data.aws_kms_key.storage_encryption_key[0].valid_to : null
}

output "storage_encryption_key_expiration_enabled" {
  value = length(data.aws_kms_key.storage_encryption_key) == 1 ? data.aws_kms_key.storage_encryption_key[0].expiration_model : null
}

output "storage_encryption_key_multi_region_config" {
  value = length(data.aws_kms_key.storage_encryption_key) == 1 ? data.aws_kms_key.storage_encryption_key[0].multi_region_configuration : null
}

output "storage_encryption_key_multi_region_enabled" {
  value = length(data.aws_kms_key.storage_encryption_key) == 1 ? data.aws_kms_key.storage_encryption_key[0].multi_region : null
}

################################################################################


