################################################################################
# Optional Variables
################################################################################

variable "route53_records_target_health" {
  type        = bool
  default     = false
  description = "Set to 'true' if you want Route 53 to determine whether to respond to DNS queries using this resource record set by checking the health of the resource record set."
}

################################################################################

variable "route53_records_ipv4_type" {
  type        = string
  default     = "A"
  description = "The IPv4 record type. This should always be 'A'."
}

variable "route53_records_ipv4_create" {
  type        = bool
  default     = false
  description = "Should IPv4 DNS records be created for this RDS instance."
}

################################################################################
# Locals
################################################################################

locals {
  route53_records_suffix = join("", data.aws_route53_zone.scope.*.name)
}

################################################################################
# Resources
################################################################################

resource "aws_route53_record" "ipv4" {
  count   = var.route53_records_ipv4_create ? 1 : 0
  name    = format("%s.%s", var.name, local.route53_records_suffix)
  type    = var.route53_records_ipv4_type
  zone_id = data.aws_route53_zone.scope[0].name

  alias {
    name                   = aws_db_instance.scope.address
    zone_id                = aws_db_instance.scope.hosted_zone_id
    evaluate_target_health = var.route53_records_target_health
  }
}

################################################################################
# Outputs
################################################################################

output "route53_records_target_health" {
  value = var.route53_records_target_health
}

################################################################################

output "route53_records_ipv4_create" {
  value = var.route53_records_ipv4_create
}

################################################################################

output "route53_records_ipv4_name" {
  value = length(aws_route53_record.ipv4) == 1 ? aws_route53_record.ipv4[0].name : null
}
output "route53_records_ipv4_type" {
  value = length(aws_route53_record.ipv4) == 1 ? aws_route53_record.ipv4[0].type : null
}

################################################################################
