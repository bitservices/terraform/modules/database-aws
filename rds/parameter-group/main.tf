################################################################################
# Required Variables
################################################################################

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "family" {
  type        = string
  description = "The database family these parameters apply to."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

################################################################################
# Optional Variables
################################################################################

variable "class" {
  type        = string
  default     = "default"
  description = "Identifier for this database parameter group."
}

variable "description" {
  type        = string
  default     = "Managed by Terraform"
  description = "The description of the database parameter group."
}

################################################################################

variable "defaults" {
  description = "A built-in set of default parameters for each supported database family. Ignored if 'parameters' is specified."

  type = map(list(object({
    name   = string
    value  = string
    method = optional(string, "immediate")
  })))

  default = {
    "mysql8.0" = [
      {
        "name"  = "log_output"
        "value" = "FILE"
      },
      {
        "name"  = "sync_binlog"
        "value" = "0"
      },
      {
        "name"  = "slow_query_log"
        "value" = "1"
      },
      {
        "name"  = "tmp_table_size"
        "value" = "67108864"
      },
      {
        "name"  = "long_query_time"
        "value" = "2"
      },
      {
        "name"  = "table_open_cache"
        "value" = "25000"
      },
      {
        "name"  = "innodb_io_capacity"
        "value" = "2000"
      },
      {
        "name"   = "innodb_open_files"
        "value"  = "4000"
        "method" = "pending-reboot"
      },
      {
        "name"  = "max_allowed_packet"
        "value" = "1073741824"
      },
      {
        "name"   = "performance_schema"
        "value"  = "1"
        "method" = "pending-reboot"
      },
      {
        "name"  = "max_heap_table_size"
        "value" = "67108864"
      },
      {
        "name"   = "innodb_log_file_size"
        "value"  = "536870912"
        "method" = "pending-reboot"
      },
      {
        "name"  = "table_definition_cache"
        "value" = "4000"
      },
      {
        "name"   = "innodb_read_io_threads"
        "value"  = "16"
        "method" = "pending-reboot"
      },
      {
        "name"  = "innodb_lock_wait_timeout"
        "value" = "120"
      },
      {
        "name"   = "innodb_write_io_threads"
        "value"  = "16"
        "method" = "pending-reboot"
      },
      {
        "name"  = "require_secure_transport"
        "value" = "1"
      },
      {
        "name"  = "innodb_flush_log_at_trx_commit"
        "value" = "0"
      },
      {
        "name"  = "log_bin_trust_function_creators"
        "value" = "1"
      }
    ]

    "mysql5.7" = [
      {
        "name"  = "log_output"
        "value" = "FILE"
      },
      {
        "name"  = "sync_binlog"
        "value" = "0"
      },
      {
        "name"  = "slow_query_log"
        "value" = "1"
      },
      {
        "name"  = "tmp_table_size"
        "value" = "67108864"
      },
      {
        "name"  = "long_query_time"
        "value" = "2"
      },
      {
        "name"  = "query_cache_size"
        "value" = "0"
      },
      {
        "name"  = "collation_server"
        "value" = "utf8mb4_unicode_ci"
      },
      {
        "name"  = "table_open_cache"
        "value" = "25000"
      },
      {
        "name"   = "innodb_open_files"
        "value"  = "4000"
        "method" = "pending-reboot"
      },
      {
        "name"  = "innodb_io_capacity"
        "value" = "2000"
      },
      {
        "name"  = "max_allowed_packet"
        "value" = "1073741824"
      },
      {
        "name"  = "max_heap_table_size"
        "value" = "67108864"
      },
      {
        "name"   = "performance_schema"
        "value"  = "1"
        "method" = "pending-reboot"
      },
      {
        "name"  = "character_set_client"
        "value" = "utf8mb4"
      },
      {
        "name"  = "character_set_server"
        "value" = "utf8mb4"
      },
      {
        "name"  = "collation_connection"
        "value" = "utf8mb4_unicode_ci"
      },
      {
        "name"  = "character_set_results"
        "value" = "utf8mb4"
      },
      {
        "name"   = "innodb_log_file_size"
        "value"  = "536870912"
        "method" = "pending-reboot"
      },
      {
        "name"  = "character_set_database"
        "value" = "utf8mb4"
      },
      {
        "name"  = "table_definition_cache"
        "value" = "4000"
      },
      {
        "name"   = "innodb_read_io_threads"
        "value"  = "16"
        "method" = "pending-reboot"
      },
      {
        "name"   = "innodb_write_io_threads"
        "value"  = "16"
        "method" = "pending-reboot"
      },
      {
        "name"  = "innodb_lock_wait_timeout"
        "value" = "120"
      },
      {
        "name"  = "require_secure_transport"
        "value" = "1"
      },
      {
        "name"  = "character_set_connection"
        "value" = "utf8mb4"
      },
      {
        "name"  = "character_set_filesystem"
        "value" = "utf8mb4"
      },
      {
        "name"  = "innodb_max_dirty_pages_pct"
        "value" = "90"
      },
      {
        "name"  = "innodb_flush_log_at_trx_commit"
        "value" = "0"
      },
      {
        "name"  = "log_bin_trust_function_creators"
        "value" = "1"
      }
    ]

    "mysql5.6" = [
      {
        "name"  = "sql_mode"
        "value" = "NO_ENGINE_SUBSTITUTION,STRICT_TRANS_TABLES"
      },
      {
        "name"  = "log_output"
        "value" = "FILE"
      },
      {
        "name"  = "sync_binlog"
        "value" = "0"
      },
      {
        "name"  = "log_warnings"
        "value" = "2"
      },
      {
        "name"  = "slow_query_log"
        "value" = "1"
      },
      {
        "name"  = "tmp_table_size"
        "value" = "67108864"
      },
      {
        "name"  = "long_query_time"
        "value" = "2"
      },
      {
        "name"  = "query_cache_size"
        "value" = "0"
      },
      {
        "name"  = "collation_server"
        "value" = "utf8mb4_unicode_ci"
      },
      {
        "name"  = "table_open_cache"
        "value" = "25000"
      },
      {
        "name"  = "innodb_file_format"
        "value" = "Barracuda"
      },
      {
        "name"   = "innodb_open_files"
        "value"  = "4000"
        "method" = "pending-reboot"
      },
      {
        "name"  = "innodb_io_capacity"
        "value" = "2000"
      },
      {
        "name"  = "max_allowed_packet"
        "value" = "1073741824"
      },
      {
        "name"  = "max_heap_table_size"
        "value" = "67108864"
      },
      {
        "name"   = "performance_schema"
        "value"  = "1"
        "method" = "pending-reboot"
      },
      {
        "name"  = "character_set_client"
        "value" = "utf8mb4"
      },
      {
        "name"  = "character_set_server"
        "value" = "utf8mb4"
      },
      {
        "name"  = "collation_connection"
        "value" = "utf8mb4_unicode_ci"
      },
      {
        "name"  = "character_set_results"
        "value" = "utf8mb4"
      },
      {
        "name"   = "innodb_log_file_size"
        "value"  = "536870912"
        "method" = "pending-reboot"
      },
      {
        "name"  = "character_set_database"
        "value" = "utf8mb4"
      },
      {
        "name"   = "innodb_read_io_threads"
        "value"  = "16"
        "method" = "pending-reboot"
      },
      {
        "name"  = "table_definition_cache"
        "value" = "4000"
      },
      {
        "name"  = "character_set_connection"
        "value" = "utf8mb4"
      },
      {
        "name"  = "character_set_filesystem"
        "value" = "utf8mb4"
      },
      {
        "name"   = "innodb_write_io_threads"
        "value"  = "16"
        "method" = "pending-reboot"
      },
      {
        "name"  = "innodb_lock_wait_timeout"
        "value" = "120"
      },
      {
        "name"  = "innodb_max_dirty_pages_pct"
        "value" = "90"
      },
      {
        "name"  = "innodb_flush_log_at_trx_commit"
        "value" = "0"
      },
      {
        "name"  = "log_bin_trust_function_creators"
        "value" = "1"
      }
    ]
  }
}

variable "parameters" {
  description = "A user provided set of parameters for the specified database family."
  default     = null

  type = list(object({
    name   = string
    value  = string
    method = optional(string, "immediate")
  }))
}

################################################################################
# Locals
################################################################################

locals {
  name       = format("%s-%s", replace(lower(var.family), "/[._]/", "-"), var.class)
  parameters = coalesce(var.parameters, lookup(var.defaults, var.family, []))
}

################################################################################
# Resources
################################################################################

resource "aws_db_parameter_group" "scope" {
  name        = local.name
  family      = var.family
  description = var.description

  tags = {
    "Name"    = local.name
    "Class"   = var.class
    "Owner"   = var.owner
    "Family"  = var.family
    "Company" = var.company
  }

  dynamic "parameter" {
    for_each = local.parameters

    content {
      name         = parameter.value.name
      value        = parameter.value.value
      apply_method = parameter.value.method
    }
  }
}

################################################################################
# Outputs
################################################################################

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

################################################################################

output "class" {
  value = var.class
}

output "description" {
  value = var.description
}

################################################################################

output "defaults" {
  value = var.defaults
}

output "parameters" {
  value = local.parameters
}

################################################################################

output "id" {
  value = aws_db_parameter_group.scope.id
}

output "arn" {
  value = aws_db_parameter_group.scope.arn
}

output "name" {
  value = aws_db_parameter_group.scope.name
}

output "family" {
  value = aws_db_parameter_group.scope.family
}

################################################################################
