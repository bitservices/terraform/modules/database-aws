<!----------------------------------------------------------------------------->

# rds/parameter-group

#### [RDS] Parameter group for multiple engine types and versions

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/database/aws//rds/parameter-group`**

--------------------------------------------------------------------------------

### Example Usage

```
module "my_parameter_group" {
  source  = "gitlab.com/bitservices/database/aws//rds/parameter-group"
  owner   = "terraform@bitservices.io"
  family  = "mysql8.0"
  company = "BITServices Ltd"
}
```

<!----------------------------------------------------------------------------->

[RDS]: https://aws.amazon.com/rds

<!----------------------------------------------------------------------------->
