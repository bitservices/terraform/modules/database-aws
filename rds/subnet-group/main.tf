################################################################################
# Required Variables
################################################################################

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

################################################################################
# Optional Variables
################################################################################

variable "class" {
  type        = string
  default     = "default"
  description = "This forms the name of the RDS subnet group, this is appended to 'vpc', 'subnet_tier' and 'subnet_set' to form the name of the RDS subnet group."
}

################################################################################
# Locals
################################################################################

locals {
  name = format("%s-%s-%s-%s", var.vpc, var.subnet_tier, var.subnet_set, var.class)
}

################################################################################
# Resources
################################################################################

resource "aws_db_subnet_group" "scope" {
  name       = local.name
  subnet_ids = local.subnet_ids

  tags = {
    Set     = var.subnet_set
    VPC     = var.vpc
    Name    = local.name
    Tier    = var.subnet_tier
    Owner   = var.owner
    Region  = data.aws_region.scope.name
    Company = var.company
  }
}

################################################################################
# Outputs
################################################################################

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

################################################################################

output "class" {
  value = var.class
}

################################################################################

output "id" {
  value = aws_db_subnet_group.scope.id
}

output "arn" {
  value = aws_db_subnet_group.scope.arn
}

output "name" {
  value = aws_db_subnet_group.scope.name
}

output "tags" {
  value = aws_db_subnet_group.scope.tags_all
}

################################################################################
