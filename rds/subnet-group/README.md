<!----------------------------------------------------------------------------->

# rds/subnet-group

#### A group of subnets that can be used by an [RDS] instance

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/database/aws//rds/subnet-group`**

--------------------------------------------------------------------------------

### Example Usage

```
module "my_subnet_group" {
  source  = "gitlab.com/bitservices/database/aws//rds/subnet-group"
  vpc     = "sandpit01"
  owner   = "terraform@bitservices.io"
  company = "BITServices Ltd"
}
```

<!----------------------------------------------------------------------------->

[RDS]: https://aws.amazon.com/rds

<!----------------------------------------------------------------------------->
