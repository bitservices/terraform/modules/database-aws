<!----------------------------------------------------------------------------->

# database (aws)

<!----------------------------------------------------------------------------->

## Description

Manage [AWS] database services, such as [RDS] and [DynamoDB].

<!----------------------------------------------------------------------------->

## Modules

* [dynamodb/autoscaling-table](dynamodb/autoscaling-table/README.md) - A [DynamoDB] table with auto-scaling enabled.
* [rds/certificate](rds/certificate/README.md) - Gathers certificate authority information for [RDS].
* [rds/instance](rds/instance/README.md) - Manage [RDS] database instances.
* [rds/parameter-group](rds/parameter-group/README.md) - [RDS] Parameter group for multiple engine types and versions.
* [rds/subnet-group](rds/subnet-group) - Subnet group for use with [RDS] instances.

<!----------------------------------------------------------------------------->

[AWS]:      https://aws.amazon.com/
[RDS]:      https://aws.amazon.com/rds/
[DynamoDB]: https://aws.amazon.com/dynamodb/

<!----------------------------------------------------------------------------->
