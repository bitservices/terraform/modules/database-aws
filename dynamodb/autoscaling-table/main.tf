################################################################################
# Required Variables
################################################################################

variable "class" {
  type        = string
  description = "This forms the name of the DynamoDB table. Unless 'name' is set, it is prefixed to the region and 'account' (if specified)."
}

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

################################################################################
# Optional Variables
################################################################################

variable "name" {
  type        = string
  default     = null
  description = "The full name of this DynamoDB table. Generated with 'class', the region and 'account' (if specified) by default."
}

variable "account" {
  type        = string
  default     = null
  description = "The account name to be appended to the automatically generated DynamoDB table name to ensure uniqueness. Ignored if not set or 'name' is specified."
}

################################################################################

variable "hash_key_name" {
  type        = string
  default     = "ID"
  description = "The attribute to use as the hash key."
}

variable "hash_key_type" {
  type        = string
  default     = "S"
  description = "The type of 'hash_key_name', must be (S)tring, (N)umber or (B)inary."
}

################################################################################

variable "read_capacity_min" {
  type        = number
  default     = 2
  description = "The minimum read capacity in DynamoDB IO units."
}

variable "read_capacity_max" {
  type        = number
  default     = 1000
  description = "The maximum read capacity in DynamoDB IO units."
}

variable "read_target_utilisation" {
  type        = number
  default     = 70
  description = "The target read utilisation of the DynamoDB table in percent."
}

################################################################################

variable "write_capacity_min" {
  type        = number
  default     = 0
  description = "The minimum write capacity in DynamoDB IO units. Defaults to 'read_capacity_min'."
}

variable "write_capacity_max" {
  type        = number
  default     = 0
  description = "The maximum write capacity in DynamoDB IO units. Defaults to 'read_capacity_max'."
}

variable "write_target_utilisation" {
  type        = number
  default     = 0
  description = "The target write utilisation of the DynamoDB table in percent. Defaults to 'read_target_utilisation'."
}

################################################################################
# Locals
################################################################################

locals {
  name                     = coalesce(var.name, format("%s-%s%s", var.class, data.aws_region.scope.name, var.account != null ? format("-%s", var.account) : ""))
  write_capacity_min       = var.write_capacity_min == 0 ? var.read_capacity_min : var.write_capacity_min
  write_capacity_max       = var.write_capacity_max == 0 ? var.read_capacity_max : var.write_capacity_max
  write_target_utilisation = var.write_target_utilisation == 0 ? var.read_target_utilisation : var.write_target_utilisation
}

################################################################################
# Resources
################################################################################

resource "aws_dynamodb_table" "scope" {
  name           = local.name
  read_capacity  = var.read_capacity_min
  write_capacity = local.write_capacity_min
  hash_key       = var.hash_key_name

  tags = {
    Name    = local.name
    Class   = var.class
    Owner   = var.owner
    Region  = data.aws_region.scope.name
    Company = var.company
  }

  attribute {
    name = var.hash_key_name
    type = var.hash_key_type
  }

  lifecycle {
    ignore_changes = [read_capacity,
    write_capacity]
  }
}

################################################################################
# Outputs
################################################################################

output "class" {
  value = var.class
}

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

################################################################################

output "account" {
  value = var.account
}

################################################################################

output "hash_key_name" {
  value = var.hash_key_name
}

output "hash_key_type" {
  value = var.hash_key_type
}

################################################################################

output "read_target_utilisation" {
  value = var.read_target_utilisation
}

output "write_target_utilisation" {
  value = local.write_target_utilisation
}

################################################################################

output "arn" {
  value = aws_dynamodb_table.scope.arn
}

output "name" {
  value = aws_dynamodb_table.scope.id
}

################################################################################
