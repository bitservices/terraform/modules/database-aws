################################################################################
# Optional Variables
################################################################################

variable "autoscaling_namespace" {
  type        = string
  default     = "dynamodb"
  description = "The AWS service namespace of the scalable target."
}

variable "autoscaling_resource_type" {
  type        = string
  default     = "table"
  description = "The autoscaling resource type. This makes up the autoscaling resource identifer."
}

################################################################################

variable "autoscaling_policy_type" {
  type        = string
  default     = "TargetTrackingScaling"
  description = "The autoscaling policy type. For DynamoDB, only 'TargetTrackingScaling' is supported."
}

################################################################################

variable "autoscaling_read_dimension" {
  type        = string
  default     = "ReadCapacityUnits"
  description = "The scalable dimension of the read capacity of the DynamoDB table."
}

variable "autoscaling_read_metric_type" {
  type        = string
  default     = "DynamoDBReadCapacityUtilization"
  description = "The predefined metric for DynamoDB read capacity."
}

################################################################################

variable "autoscaling_write_dimension" {
  type        = string
  default     = "WriteCapacityUnits"
  description = "The scalable dimension of the write capacity of the DynamoDB table."
}

variable "autoscaling_write_metric_type" {
  type        = string
  default     = "DynamoDBWriteCapacityUtilization"
  description = "The predefined metric for DynamoDB write capacity."
}

################################################################################
# Locals
################################################################################

locals {
  autoscaling_read_target_dimension  = format("%s:%s:%s", var.autoscaling_namespace, var.autoscaling_resource_type, var.autoscaling_read_dimension)
  autoscaling_write_target_dimension = format("%s:%s:%s", var.autoscaling_namespace, var.autoscaling_resource_type, var.autoscaling_write_dimension)
}

################################################################################
# Resources
################################################################################

resource "aws_appautoscaling_target" "read_target" {
  min_capacity       = var.read_capacity_min
  max_capacity       = var.read_capacity_max
  resource_id        = format("%s/%s", var.autoscaling_resource_type, aws_dynamodb_table.scope.id)
  role_arn           = data.aws_iam_role.scope.arn
  scalable_dimension = local.autoscaling_read_target_dimension
  service_namespace  = var.autoscaling_namespace
}

resource "aws_appautoscaling_policy" "read_policy" {
  name               = format("%s:%s", var.autoscaling_read_metric_type, aws_appautoscaling_target.read_target.resource_id)
  policy_type        = var.autoscaling_policy_type
  resource_id        = aws_appautoscaling_target.read_target.resource_id
  scalable_dimension = aws_appautoscaling_target.read_target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.read_target.service_namespace

  target_tracking_scaling_policy_configuration {
    target_value = var.read_target_utilisation

    predefined_metric_specification {
      predefined_metric_type = var.autoscaling_read_metric_type
    }
  }
}

################################################################################

resource "aws_appautoscaling_target" "write_target" {
  min_capacity       = local.write_capacity_min
  max_capacity       = local.write_capacity_max
  resource_id        = format("%s/%s", var.autoscaling_resource_type, aws_dynamodb_table.scope.id)
  role_arn           = data.aws_iam_role.scope.arn
  scalable_dimension = local.autoscaling_write_target_dimension
  service_namespace  = var.autoscaling_namespace
}

resource "aws_appautoscaling_policy" "write_policy" {
  name               = format("%s:%s", var.autoscaling_write_metric_type, aws_appautoscaling_target.write_target.resource_id)
  policy_type        = var.autoscaling_policy_type
  resource_id        = aws_appautoscaling_target.write_target.resource_id
  scalable_dimension = aws_appautoscaling_target.write_target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.write_target.service_namespace

  target_tracking_scaling_policy_configuration {
    target_value = local.write_target_utilisation

    predefined_metric_specification {
      predefined_metric_type = var.autoscaling_write_metric_type
    }
  }
}

################################################################################
# Outputs
################################################################################

output "autoscaling_resource_type" {
  value = var.autoscaling_resource_type
}

################################################################################

output "autoscaling_read_metric_type" {
  value = var.autoscaling_read_metric_type
}

output "autoscaling_write_metric_type" {
  value = var.autoscaling_write_metric_type
}

################################################################################

output "autoscaling_read_target_min" {
  value = aws_appautoscaling_target.read_target.min_capacity
}

output "autoscaling_read_target_max" {
  value = aws_appautoscaling_target.read_target.max_capacity
}

output "autoscaling_read_target_resource" {
  value = aws_appautoscaling_target.read_target.resource_id
}

output "autoscaling_read_target_dimension" {
  value = aws_appautoscaling_target.read_target.scalable_dimension
}

output "autoscaling_read_target_namespace" {
  value = aws_appautoscaling_target.read_target.service_namespace
}

################################################################################

output "autoscaling_read_policy_arn" {
  value = aws_appautoscaling_policy.read_policy.arn
}

output "autoscaling_read_policy_name" {
  value = aws_appautoscaling_policy.read_policy.name
}

output "autoscaling_read_policy_type" {
  value = aws_appautoscaling_policy.read_policy.policy_type
}

################################################################################

output "autoscaling_write_target_min" {
  value = aws_appautoscaling_target.write_target.min_capacity
}

output "autoscaling_write_target_max" {
  value = aws_appautoscaling_target.write_target.max_capacity
}

output "autoscaling_write_target_resource" {
  value = aws_appautoscaling_target.write_target.resource_id
}

output "autoscaling_write_target_dimension" {
  value = aws_appautoscaling_target.write_target.scalable_dimension
}

output "autoscaling_write_target_namespace" {
  value = aws_appautoscaling_target.write_target.service_namespace
}

################################################################################

output "autoscaling_write_policy_arn" {
  value = aws_appautoscaling_policy.write_policy.arn
}

output "autoscaling_write_policy_name" {
  value = aws_appautoscaling_policy.write_policy.name
}

output "autoscaling_write_policy_type" {
  value = aws_appautoscaling_policy.write_policy.policy_type
}

################################################################################
