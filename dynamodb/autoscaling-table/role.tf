################################################################################
# Optional Variables
################################################################################

variable "role_dynamodb_autoscaling" {
  type        = string
  default     = "AWSServiceRoleForApplicationAutoScaling_DynamoDBTable"
  description = "Name of the built-in Amazon Web Services IAM role for DynamoDB table autoscaling."
}

################################################################################
# Data Sources
################################################################################

data "aws_iam_role" "scope" {
  name = var.role_dynamodb_autoscaling
}

################################################################################
# Outputs
################################################################################

output "role_id" {
  value = data.aws_iam_role.scope.unique_id
}

output "role_arn" {
  value = data.aws_iam_role.scope.arn
}

output "role_name" {
  value = data.aws_iam_role.scope.id
}

output "role_path" {
  value = data.aws_iam_role.scope.path
}

output "role_policy" {
  value = data.aws_iam_role.scope.assume_role_policy
}

################################################################################
