<!----------------------------------------------------------------------------->

# dynamodb/autoscaling-table

#### A [DynamoDB] table with auto-scaling enabled

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/database/aws//dynamodb/autoscaling-table`**

--------------------------------------------------------------------------------

### Example Usage

```
module "my_dynamodb_table" {
  source  = "gitlab.com/bitservices/database/aws//dynamodb/autoscaling-table"
  class   = "nosql"
  owner   = "terraform@bitservices.io"
  company = "BITServices Ltd"
}
```

<!----------------------------------------------------------------------------->

[DynamoDB]: https://aws.amazon.com/dynamodb/

<!----------------------------------------------------------------------------->
