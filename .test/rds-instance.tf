################################################################################
# Resources
################################################################################

resource "random_password" "rds_instance" {
  length  = 32
  special = false
}

################################################################################
# Modules
################################################################################

module "rds_instance" {
  source            = "../rds/instance"
  vpc               = local.vpc
  name              = "foobar"
  owner             = local.owner
  company           = local.company
  username          = "username"
  password          = random_password.rds_instance.result
  parameters        = module.rds_parameter_group.name
  subnet_group_name = module.rds_subnet_group.name
}

################################################################################
