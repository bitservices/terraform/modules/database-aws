################################################################################
# Terraform Settings
################################################################################

terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/aws"
    }

    http = {
      source = "hashicorp/http"
    }

    random = {
      source = "hashicorp/random"
    }
  }
}

################################################################################
